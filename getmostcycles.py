# dictionary lazy cache to help with computation
cache={}


# (a, b => x), (c,d => y), (e,f => z)

# i, j = (500,2500)
# (1000 = x), (2000 = y), (3000 = z)

#copy of collatz_eval_simple in Collatz.py
def collatz_eval(i, j):
	max = 1
	if i > j:
		start = j
		end = i
	else:
		start = i
		end = j
	
	for n in range(start, end + 1):
		temp = collatz_find(n, cache)
		cache[n] = temp
		if temp > max:
			max = temp
	return max

#copy of collatz_find in collatz.py
def collatz_find(n, cache):
	curr = 1
	while n != 1:

        # if n in freq_cache:
        #     freq_cache[n] += 1
        # else:
        #     freq_cache[n] = 1

		if n in cache:
			curr += cache[n] - 1
			break
		if n % 2 == 0:
			n /= 2
		else:
			n = 3 * n + 1
		curr += 1

	return curr

#generate array of tuples with format (x, max_cycle_length(x-999, x))
#have each x be multiples of 1000 and stop at 1000000
def init_range_arr():
	vals_list = []
	for item in range(1000, 1000001, 1000):
		max_cycle_length = collatz_eval(item - 999, item)
		vals_list.append((item, max_cycle_length))
	print(vals_list)

init_range_arr()