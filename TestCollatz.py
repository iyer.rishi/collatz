#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import cache, collatz_read, collatz_eval, collatz_eval_simple, collatz_find, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_empty_line(self):
        s = "\n"
        self.assertEqual(collatz_read(s), "\n")

    def test_floats(self):
        s = "105 252\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 105)
        self.assertEqual(j, 252)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_reverse_input(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_string(self):
        w = StringIO()
        collatz_print(w, "string1", "string2", "string3")
        self.assertEqual(w.getvalue(), "string1 string2 string3\n")

    def test_bool(self):
        w = StringIO()
        collatz_print(w, True, False, False)
        self.assertEqual(w.getvalue(), "True False False\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n1 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n1 999999 525\n")

    def test_empty_lines(self):
        r = StringIO("1 10\n100 200\n\n\n201 210\n900 1000\n1 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n\n\n201 210 89\n900 1000 174\n1 999999 525\n")

    def test_multiple_empties(self):
        r = StringIO("1 10\n100 200\n201 210\n\n\n900 1000\n1 999999\n\n\n\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n\n\n900 1000 174\n1 999999 525\n\n\n\n")

    # ----
    # simple
    # ----

    def test_simple_1(self):
        v = collatz_eval_simple(1, 10)
        self.assertEqual(v, 20)

    def test_simple_2(self):
        v = collatz_eval_simple(100, 200)
        self.assertEqual(v, 125)

    def test_simple_3(self):
        v = collatz_eval_simple(201, 210)
        self.assertEqual(v, 89)

    def test_simple_4(self):
        v = collatz_eval_simple(900, 1000)
        self.assertEqual(v, 174)

    # ----
    # find
    # ----

    def test_find_1(self):
        v = collatz_find(1, cache)
        self.assertEqual(v, 1)

    def test_find_2(self):
        v = collatz_find(910107, cache)
        self.assertEqual(v, 476)

    def test_find_3(self):
        v = collatz_find(200519, cache)
        self.assertEqual(v, 311)

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
